#addin nuget:?package=Cake.VersionReader&version=5.1.0
#addin nuget:?package=Newtonsoft.Json&version=13.0.1
using Newtonsoft.Json;
//////////////////////////////////////////////////////////////////////
// CONFIGURATION
//////////////////////////////////////////////////////////////////////
const string ProjectName = "TheDot";
var PluginInfo = new DalamudPluginInfo
{
    Author = "silusffxi",
    Name = "The Dot",
    Punchline = "Shows the approximate location of your hitbox.",
    Description = "Shows the approximate location of your hitbox. Acts as a replacement for Pixel Perfect.",
    Tags = new List<string>
    {
        "Combat",
        "UI"
    },
    InternalName = ProjectName,
    RepoUrl = "https://gitlab.com/silusffxi/dalamud-the-dot",
    LastUpdate = DateTimeOffset.Now.ToUnixTimeSeconds().ToString(),
    DownloadLinkInstall = "https://gitlab.com/silusffxi/dalamud-plugins/-/raw/main/TheDot/latest.zip",
    DownloadLinkTesting = "https://gitlab.com/silusffxi/dalamud-plugins/-/raw/main/TheDot/latest.zip",
    DownloadLinkUpdate = "https://gitlab.com/silusffxi/dalamud-plugins/-/raw/main/TheDot/latest.zip",
};

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////
var BuildConfiguration = Argument("configuration", "Release");
var BuildTarget = Argument("target", "Default");

//////////////////////////////////////////////////////////////////////
// SCRIPT VARS
//////////////////////////////////////////////////////////////////////
var RootDir = new DirectoryPath(System.IO.Directory.GetCurrentDirectory());
var ProjectDir = new DirectoryPath(RootDir + $"/{ProjectName}");
var BuildDir = new DirectoryPath(RootDir + "/build");
var BinDir = new DirectoryPath(BuildDir + "/bin");
var PackageDir = new DirectoryPath(BuildDir + "/package");

// These vars refer to another repository that should be nearby in the directory structure.
// This will let the build script generate the appropriate entries 
var PluginsRoot = new DirectoryPath(System.IO.Path.GetFullPath(RootDir + "/.."));
var PluginsRepoDir = new DirectoryPath(System.IO.Path.GetFullPath(PluginsRoot + "/dalamud-plugins"));
var PluginRepoPackageDir = new DirectoryPath(System.IO.Path.GetFullPath(PluginsRepoDir + $"/{ProjectName}"));

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////
Task("Clean")
    .Does(() =>
    {
        CleanDirectory(BuildDir);
        CleanDirectory(PackageDir);
    });

Task("Build")
    .IsDependentOn("Clean")
    .Does(() =>
    {
        var projPath = new FilePath(ProjectDir + $"/{ProjectName}.csproj");
        DotNetBuild(projPath.ToString(), new DotNetBuildSettings
        {
            Configuration = BuildConfiguration,
            OutputDirectory = BinDir,
            Runtime = "net5.0-windows",
        });

        PluginInfo.AssemblyVersion = GetVersionNumber(BinDir + $"/{ProjectName}.dll");
    });

Task("Package")
    .IsDependentOn("Build")
    .Does(() =>
    {
        Zip(BinDir, PackageDir + "/latest.zip", BinDir + "/*.*");
    });

Task("Update-Plugin-Repository")
    .IsDependentOn("Package")
    .Does(() =>
    {
        if (DirectoryExists(PluginsRepoDir))
        {
            EnsureDirectoryExists(PluginRepoPackageDir);
            CopyFile(PackageDir + "/latest.zip", PluginRepoPackageDir + "/latest.zip");

            var pluginInfoFile = new FilePath(PluginRepoPackageDir + "/plugin.json");
            var pluginInfoJson = JsonConvert.SerializeObject(PluginInfo);
            System.IO.File.WriteAllText(pluginInfoFile.ToString(), pluginInfoJson);
        }
        else
        {
            Warning("Plugin repository repo does not appear to be cloned locally. Plugin repoistory will not be updated with this build.");
        }
    });

Task("Default")
    .IsDependentOn("Update-Plugin-Repository");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(BuildTarget);

class DalamudPluginInfo
{
    public string Author { get; set;}

    public string Name { get; set; }

    public string Punchline { get; set; }

    public string Description { get; set; }

    public List<string> Tags { get; set; }

    public string InternalName { get; set; }

    public string AssemblyVersion { get; set; }

    public string RepoUrl { get; set; }

    public string ApplicableVersion { get; set; } = "any";

    public int DalamudApiLevel { get; set; } = 6;

    public int LoadPriority { get; set; } = 0;

    public List<string> ImageUrls { get; set; }

    public string IconUrl { get; set; }

    public bool IsHide { get; set; } = false;

    public bool IsTestingExclusive { get; set; } = false;

    public int DownloadCount { get; set; } = 0;

    public string LastUpdate { get; set; } = "0";

    public string DownloadLinkInstall { get; set; }

    public string DownloadLinkTesting { get; set; }

    public string DownloadLinkUpdate { get; set; }
};