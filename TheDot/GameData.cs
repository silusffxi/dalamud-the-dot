using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Conditions;
using Dalamud.Game.ClientState.Objects.SubKinds;

namespace TheDot
{
    public class GameData
    {
        public PlayerCharacter Player => Client?.LocalPlayer;

        public bool PlayerLoggedIn => Client?.IsLoggedIn ?? false;

        public bool InCombat => Cond != null && Cond[ConditionFlag.InCombat];

        public bool InInstance => Cond != null && Cond[ConditionFlag.BoundByDuty];

        private ClientState Client { get; }

        private Condition Cond { get; }

        public GameData(ClientState clientState, Condition condition)
        {
            Client = clientState;
            Cond = condition;            
        }
    }
}
