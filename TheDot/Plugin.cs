using System;
using System.Formats.Asn1;
using System.Numerics;
using System.Runtime.CompilerServices;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Conditions;
using Dalamud.Game.ClientState.Objects.Enums;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Game.Command;
using Dalamud.Game.Gui;
using Dalamud.Interface;
using Dalamud.Logging;
using Dalamud.Plugin;
using ImGuiNET;

namespace TheDot
{
    public class Plugin : IDalamudPlugin
    {
        public const string PLUGIN_NAME = "The Dot";

        private const string PLUGIN_COMMAND = "/dot";

        private static Vector2 ZeroVec2 { get; } = new(0.0f, 0.0f);

        public string Name => PLUGIN_NAME;

        public bool Disposed { get; private set; }

        private DalamudPluginInterface PluginInterface { get; }

        private CommandManager CmdManager { get; }

        private Framework Framework { get; }

        private GameGui Gui { get; }

        private GameData GameData { get; }

        private PluginConfig _config;

        private ConfigWindow _configWindow;

        public Plugin(DalamudPluginInterface pluginInterface, CommandManager cmdManager,
            ClientState clientState, Framework framework,
            GameGui gameGui, Condition condition)
        {
            PluginInterface = pluginInterface;
            CmdManager = cmdManager;
            Framework = framework;
            Gui = gameGui;

            GameData = new GameData(clientState, condition);

            CmdManager.AddHandler("/dot", new CommandInfo(HandleCommand)
            {
                HelpMessage = "Shows the configuration for The Dot.",
                ShowInHelp = true,
            });

            _config = PluginInterface.GetPluginConfig() as PluginConfig ?? new PluginConfig();

            _configWindow = new ConfigWindow(_config, GameData);

            PluginInterface.UiBuilder.Draw += Draw;
            PluginInterface.UiBuilder.OpenConfigUi += ToggleConfigWindow;

            _configWindow.SaveConfiguration += SaveConfig;
        }

        ~Plugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Disposed) return;

            if (disposing)
            {
                CmdManager.RemoveHandler(PLUGIN_COMMAND);
                _configWindow.SaveConfiguration -= SaveConfig;
            }

            Disposed = true;
        }

        private void Draw()
        {
            DrawConfig();

            if (_config.Enabled) DrawDot();
        }

        private void DrawConfig() => _configWindow?.Draw();

        private void DrawDot()
        {
            const int segments = 100;

            if (!GameData.PlayerLoggedIn || GameData.Player == null)
                return;
            
            if (_config.VisibleInCombat && !GameData.InCombat)
                return;

            if (_config.VisibleInInstance && !GameData.InInstance)
                return;

            var pos = GameData.Player.Position;
            if (!Gui.WorldToScreen(pos, out var hitBoxPos))
                return;

            ImGui.PushStyleVar(ImGuiStyleVar.WindowPadding, ZeroVec2);
            ImGuiHelpers.ForceNextWindowMainViewport();
            ImGuiHelpers.SetNextWindowPosRelativeMainViewport(ZeroVec2);
            ImGui.Begin("dot",
                ImGuiWindowFlags.NoInputs | ImGuiWindowFlags.NoNav | ImGuiWindowFlags.NoTitleBar |
                ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoBackground);

            // Draws the actual hitbox dot.
            ImGui.SetWindowSize(ImGui.GetIO().DisplaySize);
            ImGui.GetWindowDrawList().AddCircleFilled(
                hitBoxPos,
                _config.DotSize,
                ImGui.GetColorU32(_config.DotColor),
                segments);

            // Draw the circle around the hitbox dot
            if (_config.CircleEnabled)
            {
                ImGui.GetWindowDrawList().AddCircle(
                    hitBoxPos,
                    _config.CircleRadius,
                    ImGui.GetColorU32(_config.CircleColor),
                    segments);
            }

            // Draw the ring around the hitbox area.
            if (_config.RingEnabled)
            {
                DrawRingInWorld(pos,
                    _config.RingRadius,
                    segments,
                    _config.RingLineWidth,
                    ImGui.GetColorU32(_config.RingColor));
            }

            ImGui.End();
            ImGui.PopStyleVar();
        }

        private void DrawRingInWorld(Vector3 pos, float radius, int segments, float lineWidth, uint color)
        {
            var seg = segments / 2;
            var segCircle = Math.PI / seg;

            for (var i = 0; i <= segments; i++)
            {
                var angle = segCircle * i;

                Gui.WorldToScreen(new Vector3(
                        pos.X + (radius * (float)Math.Sin(angle)),
                        pos.Y,
                        pos.Z + (radius * (float)Math.Cos(angle))),
                    out Vector2 screenPos);

                ImGui.GetWindowDrawList().PathLineTo(new Vector2(screenPos.X, screenPos.Y));
            }

            ImGui.GetWindowDrawList().PathStroke(color, ImDrawFlags.None, lineWidth);
        }

        private void HandleCommand(string cmd, string args) => ToggleConfigWindow();

        private void SaveConfig(PluginConfig config)
        {
            PluginInterface.SavePluginConfig(config);
        }

        private void ToggleConfigWindow() => _configWindow.Visible = !_configWindow.Visible;
    }
}
