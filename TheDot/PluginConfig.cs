using System.Numerics;
using Dalamud.Configuration;
using Newtonsoft.Json;

namespace TheDot
{
    public class PluginConfig : IPluginConfiguration
    {
        public bool Enabled { get; set; }

        public Vector4 DotColor { get; set; }

        public float DotSize { get; set; }

        public bool CircleEnabled { get; set; }
        
        public Vector4 CircleColor { get; set; }
        
        public float CircleRadius { get; set; }

        public bool RingEnabled { get; set; }
        
        public float RingRadius { get; set; }

        public float RingLineWidth { get; set; }

        public Vector4 RingColor { get; set; }

        public bool VisibleInCombat { get; set; }

        [JsonIgnore]
        public ref bool VisibleInCombatRef => ref _visibleInCombat;

        public bool VisibleInInstance { get; set; }

        public int Version { get; set; } = 1;

        private bool _visibleInCombat = true;

        public PluginConfig()
        {
            Enabled = true;
            DotColor = new(1.0f, 1.0f, 1.0f, 1.0f);
            DotSize = 2.0f;

            CircleEnabled = true;
            CircleColor = new(1.0f, 1.0f, 1.0f, 1.0f);
            CircleRadius = 2.2f;

            RingEnabled = false;
            RingRadius = 0.5f; // This is about the size of the target circle around the player character.
            RingLineWidth = 1.0f;
            RingColor = new(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }
}
