using System.Diagnostics;
using System.Numerics;
using ImGuiNET;

namespace TheDot
{
    public delegate void SaveConfiguration(PluginConfig config);

    public class ConfigWindow
    {
        private const string URL_PIXEL_PERFECT = "https://github.com/Haplo064/PixelPerfect";

        private static Vector4 UrlColor { get; } = new(0.0f, 0.618f, 0.871f, 1.0f);

        public event SaveConfiguration SaveConfiguration;

        public bool Visible
        {
            get => _visible;
            set => _visible = value;
        }

        private PluginConfig Config { get; }

        private GameData GameData { get; }

        private bool _visible;

        public ConfigWindow(PluginConfig config, GameData gameData)
        {
            Config = config;
            GameData = gameData;
        }

        public void Draw()
        {
            if (!Visible) return;

            if (ImGui.Begin("The Dot Configuration", ref _visible))
            {
                DrawContent();
            }

            ImGui.End();
        }

        private void DrawContent()
        {
            var enabled = Config.Enabled;
            var visibleInCombat = Config.VisibleInCombat;
            var visibleInInstance = Config.VisibleInInstance;

            ImGui.TextWrapped("Based on the Pixel Perfect plugin. All credit for the original implementation goes to Haplo064. ");
            ImGui.TextWrapped("Check out Pixel Perfect here: ");
            ImGui.SameLine();
            ImGui.SetMouseCursor(ImGuiMouseCursor.Hand);
            ImGui.TextColored(UrlColor, URL_PIXEL_PERFECT);
            if (ImGui.IsItemClicked(ImGuiMouseButton.Left))
            {
                OpenUrl(URL_PIXEL_PERFECT);
            }
            ImGui.SetMouseCursor(ImGuiMouseCursor.Arrow);

            ImGui.Separator();

            if (ImGui.Checkbox("Enabled", ref enabled))
                Config.Enabled = enabled;

            ImGui.Text("Only Visible...");
            if (ImGui.Checkbox("During Combat", ref visibleInCombat))
                Config.VisibleInCombat = visibleInCombat;

            if (ImGui.Checkbox("In Instance", ref visibleInInstance))
                Config.VisibleInInstance = visibleInInstance;

            if (ImGui.CollapsingHeader("Hitbox Dot"))
            {
                var dotSize = Config.DotSize;
                var dotColor = Config.DotColor;

                if (ImGui.DragFloat("Dot Size", ref dotSize, 1.0f, 0.1f, 1000.0f))
                    Config.DotSize = dotSize;

                if (ImGui.ColorEdit4("Color", ref dotColor))
                    Config.DotColor = dotColor;
            }

            if (ImGui.CollapsingHeader("Circle"))
            {
                var circleEnabled = Config.CircleEnabled;
                var circleColor = Config.CircleColor;
                var circleRadius = Config.CircleRadius;

                ImGui.TextWrapped("Circle that will be displayed around the hitbox dot.");

                if (ImGui.Checkbox("Show Circle", ref circleEnabled))
                    Config.CircleEnabled = circleEnabled;

                if (ImGui.DragFloat("Circle Radius", ref circleRadius, 1.0f, 0.1f, 1000.0f))
                    Config.CircleRadius = circleRadius;

                if (ImGui.ColorEdit4("Circle Color", ref circleColor))
                    Config.CircleColor = circleColor;
            }

            if (ImGui.CollapsingHeader("Ring"))
            {
                var ringEnabled = Config.RingEnabled;
                var ringRadius = Config.RingRadius;
                var ringLineWidth = Config.RingLineWidth;
                var ringColor = Config.RingColor;

                ImGui.TextWrapped("Ring around the center of your hitbox.");

                if (ImGui.Checkbox("Show Ring", ref ringEnabled))
                    Config.RingEnabled = ringEnabled;

                if (ImGui.DragFloat("Ring Radius", ref ringRadius, 0.1f, 0.1f, 30.0f))
                    Config.RingRadius = ringRadius;

                if (ImGui.DragFloat("Ring Line Width", ref ringLineWidth))
                    Config.RingLineWidth = ringLineWidth;

                if (ImGui.ColorEdit4("Ring Color", ref ringColor))
                    Config.RingColor = ringColor;
            }

            ImGui.Separator();

            if (ImGui.Button("Save"))
            {
                SaveConfiguration?.Invoke(Config);
            }

            ImGui.SameLine();
            if (ImGui.Button("Close"))
            {
                Visible = false;
                return;
            }

            ImGui.SameLine();
            if (ImGui.Button("Save & Close"))
            {
                SaveConfiguration?.Invoke(Config);
                Visible = false;
            }
        }

        private void OpenUrl(string url)
        {
            Process.Start(new ProcessStartInfo
            {
                FileName = url,
                UseShellExecute = true,
            });
        }
    }
}
